#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>
#include <QDebug>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    for(int i = 0; i < 10; i++)
    {
        ui->comboBox->addItem("Opción " + QString(QChar('A' + i)), i);
    }
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_pushButton_clicked()
{
    QString  eleccion(ui->comboBox->currentText());
    QVariant datoVar   = ui->comboBox->currentData();
    int      datoNum   = datoVar.value<int>();
    QString  datoStr   = QString::number(datoNum);
    int      indiceNum = (ui->comboBox->currentIndex());
    QString  indiceStr = QString::number(indiceNum);

    QMessageBox::information(this, "Titulo", "Texto: " + eleccion + "<br>"
                                             "Dato almacenado: " + datoStr + "<br>"
                                             "Indice de lista: " + indiceStr);
}

void Dialog::on_comboBox_currentIndexChanged(const QString &arg1)
{
    qDebug() << arg1;
}
